package com.example.demo.facade;

import com.example.demo.dto.CommentDTO;
import com.example.demo.entity.Comment;
import org.springframework.stereotype.Component;

@Component
public class CommentFacade {

    public CommentDTO commentToCommentDTO(Comment comment){
        return CommentDTO.builder()
                .username(comment.getUsername())
                .id(comment.getId())
                .message(comment.getMessage())
                .build();
    }
}

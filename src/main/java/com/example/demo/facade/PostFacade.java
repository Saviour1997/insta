package com.example.demo.facade;

import com.example.demo.dto.PostDTO;
import com.example.demo.entity.Post;
import org.springframework.stereotype.Component;

@Component
public class PostFacade {

    public PostDTO postToPostDTO(Post post){
        return PostDTO.builder()
                .id(post.getId())
                .title(post.getTitle())
                .description(post.getDescription())
                .username(post.getUser().getUsername())
                .location(post.getLocation())
                .likes(post.getLikes())
                .usersliked(post.getLikedUsers())
                .build();
    }

    public Post postDtoToPost(PostDTO postDTO){
        return Post.builder()
                .id(postDTO.getId())
                .title(postDTO.getTitle())
                .description(postDTO.getDescription())
                .location(postDTO.getLocation())
                .likes(postDTO.getLikes())
                .likedUsers(postDTO.getUsersliked())
                .build();
    }
}

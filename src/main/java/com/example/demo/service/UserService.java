package com.example.demo.service;

import com.example.demo.dto.UserDTO;
import com.example.demo.entity.User;
import com.example.demo.entity.emuns.Role;
import com.example.demo.exceptions.UserExistException;
import com.example.demo.payload.request.SignupRequest;
import com.example.demo.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Collections;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public User createUser(SignupRequest userIn) {
        User user = User.builder()
                .email(userIn.getEmail())
                .firstName(userIn.getFirstName())
                .lastName(userIn.getLastName())
                .username(userIn.getUsername())
                .password(bCryptPasswordEncoder.encode(userIn.getPassword()))
                .roles(Collections.singleton(Role.USER))
                .build();
        try {
            log.info("Saving user {}", userIn.getEmail());
            return userRepository.save(user);
        } catch (Exception ex) {
            log.error("Error during registration. {}", ex.getMessage());
            throw new UserExistException("The user " + user.getUsername() + "already exists." +
                    "Please check credentials");
        }
    }

    public User updateUser(UserDTO userDTO, Principal principal) {
        User user = getUserByPrincipal(principal);
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setBio(userDTO.getBio());

        return userRepository.save(user);
    }

    public User getUserById(Long id) {

        return userRepository.findUserById(id).orElseThrow(
                () -> new UsernameNotFoundException("User not found"));
    }

    public User getCurrentUser(Principal principal) {
        return getUserByPrincipal(principal);
    }


    public void deleteUser(Long id){
        userRepository.deleteById(id);
    }

    private User getUserByPrincipal(Principal principal) {
        String username = principal.getName();

        return userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Username not foub with username " + username));
    }
}

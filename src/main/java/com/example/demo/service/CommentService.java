package com.example.demo.service;

import com.example.demo.dto.CommentDTO;
import com.example.demo.entity.Comment;
import com.example.demo.entity.Post;
import com.example.demo.entity.User;
import com.example.demo.exceptions.CommentNotFoundException;
import com.example.demo.exceptions.PostNotFoundException;
import com.example.demo.repository.CommentRepository;
import com.example.demo.repository.PostRepository;
import com.example.demo.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class CommentService {

    private final CommentRepository commentRepository;
    private final UserRepository userRepository;
    private final PostRepository postRepository;


    public Comment saveComment(Long postId, CommentDTO commentDTO, Principal principal){
        User user = getUserByPrincipal(principal);
        Post post = postRepository.findById(postId).orElseThrow(
                ()->new PostNotFoundException("Post cannot be found for username " + user.getEmail())
        );
        Comment comment = Comment.builder()
                .post(post)
                .message(commentDTO.getMessage())
                .id(user.getId())
                .username(user.getUsername())
                .build();
        log.info("Saving comment for post : {}", post.getId());
        return commentRepository.save(comment);
    }

    public List<Comment> getAllComments(){
        return commentRepository.findAll();
    }

    public List<Comment> getAllCommentsForPost(Long postId){
        Post post = postRepository.findById(postId).orElseThrow(
                ()->new CommentNotFoundException("Post cannot be found")
        );
        return commentRepository.findAllByPost(post);
    }

    public void deleteComment(Long commentId){
        Optional<Comment> comment = commentRepository.findById(commentId);
        comment.ifPresent(commentRepository::delete);
    }


    public Comment getCommentById(Long commentId, Principal principal){
        User user = getUserByPrincipal(principal);

        return commentRepository.findByIdAndUserId(commentId, user.getId()).orElseThrow(
                ()->new CommentNotFoundException("Comment cannot be found")
        );
    }

    private User getUserByPrincipal(Principal principal) {
        String username = principal.getName();

        return userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Username not found with username " + username));
    }
}

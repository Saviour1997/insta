package com.example.demo.controller;

import com.example.demo.dto.CommentDTO;
import com.example.demo.entity.Comment;
import com.example.demo.facade.CommentFacade;
import com.example.demo.payload.response.MessageResponse;
import com.example.demo.service.CommentService;
import com.example.demo.validations.ResponseErrorValidation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("api/comment")
@RequiredArgsConstructor
public class CommentController {

    private final CommentFacade commentFacade;
    private final CommentService commentService;
    private final ResponseErrorValidation responseErrorValidation;

    @PostMapping("{postId}/create")
    public ResponseEntity<Object> createComment(@Valid @RequestBody CommentDTO commentDTO,
                                                @PathVariable("postId") String postId,
                                                BindingResult bindingResult,
                                                Principal principal) {
        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if (!ObjectUtils.isEmpty(errors)) return errors;

        Comment comment = commentService.saveComment(Long.parseLong(postId), commentDTO, principal);

        CommentDTO createNewComment = commentFacade.commentToCommentDTO(comment);

        return new ResponseEntity<>(createNewComment, HttpStatus.OK);
    }
    @GetMapping("/all")
    public ResponseEntity<List<CommentDTO>> getAllComments() {

        List<CommentDTO> getComments = commentService.getAllComments()
                .stream()
                .map(commentFacade::commentToCommentDTO)
                .toList();
        return new ResponseEntity<>(getComments, HttpStatus.OK);
    }

    @GetMapping("{postId}/all")
    public ResponseEntity<List<CommentDTO>> getAllCommentsToPost(@PathVariable String postId) {
        List<CommentDTO> allComments = commentService.getAllCommentsForPost(Long.parseLong(postId))
                .stream()
                .map(commentFacade::commentToCommentDTO)
                .toList();
        return new ResponseEntity<>(allComments, HttpStatus.OK);
    }

    @DeleteMapping("{commentId}/delete")
    public ResponseEntity<MessageResponse> deleteComment(@PathVariable String commentId){
        commentService.deleteComment(Long.parseLong(commentId));
        return new ResponseEntity<>(new MessageResponse("Comment was deleted"), HttpStatus.OK);
    }

    @GetMapping("{postId}/comment")
    public ResponseEntity<CommentDTO> getCommentById(@PathVariable String postId, Principal principal){

        Comment comment = commentService.getCommentById(Long.parseLong(postId), principal);

        CommentDTO commentById = commentFacade.commentToCommentDTO(comment);
        return new ResponseEntity<>(commentById, HttpStatus.OK);
    }
}

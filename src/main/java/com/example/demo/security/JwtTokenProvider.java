package com.example.demo.security;

import com.example.demo.entity.User;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
public class JwtTokenProvider {

    public String generateToken(Authentication authentication){
        User user = (User) authentication.getPrincipal();

        Date now = new Date(System.currentTimeMillis());
        Date expiry_time = new Date(now.getTime() + SecurityConst.EXPIRATION_TIME);

        String userId = Long.toString(user.getId());

        Map<String, Object> claimsMap = new HashMap<>();
        claimsMap.put("id", userId);
        claimsMap.put("username", user.getEmail());
        claimsMap.put("firstName", user.getFirstName());
        claimsMap.put("lastName", user.getLastName());
        return Jwts.builder()
                .setSubject(userId)
                .addClaims(claimsMap)
                .setIssuedAt(now)
                .setExpiration(expiry_time)
                .signWith(SignatureAlgorithm.HS512, SecurityConst.SECRET)
                .compact();
    }

    public boolean validateToken(String token){
        try {
            Jwts.parser()
                    .setSigningKey(SecurityConst.SECRET)
                    .parseClaimsJws(token);
            return true;
        }catch (SignatureException | MalformedJwtException | ExpiredJwtException|
                UnsupportedJwtException|IllegalArgumentException exception){
            log.error(exception.getMessage());
            return false;
        }
    }

    public Long getUserIdFromToken(String token){

        Claims claims = Jwts.parser()
                .setSigningKey(SecurityConst.SECRET)
                .parseClaimsJws(token)
                .getBody();
        String id = (String) claims.get("id");

        return Long.parseLong(id);
    }

}
